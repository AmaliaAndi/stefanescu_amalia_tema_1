function distance(first, second){

	if(Array.isArray(first) && Array.isArray(second)) {
	
	let unique1 = first.filter((o)=> second.indexOf(o) === -1);

	let unique2 = second.filter((o)=> first.indexOf(o) === -1);

	let arrayMerged =unique1.concat(unique2);

	let arrayMergedUnique = arrayMerged.filter((value, index, self) => onlyUnique(value, index, self));

	let number = arrayMergedUnique.length	

	return number

	}

	else {
		throw new UserException('InvalidType');
	}

}

function UserException(message) {
	this.message = message;
}

function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}

 

module.exports.distance = distance